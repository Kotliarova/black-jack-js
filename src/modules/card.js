
class Card {
    #value
    constructor({name, value}) {
        this.name = name;
        this.value = value;
    }
    render () {
        const card = document.createElement('div');
        card.classList.add('card');
        card.classList.add(`card-${this.name}`);
        return card;
    }

}

export default Card;