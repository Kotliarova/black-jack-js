import './index.html'
import './index.scss'
import Card from "./modules/card.js";


const allCards = [
    {name: 'ac', value: 1},
    {name: 'c2', value: 2},
    {name: '3c', value: 3},
    {name: '4c', value: 4},
    {name: '5c', value: 5},
    {name: '6c', value: 6},
    {name: '7c', value: 7}
//     '8c','9c','10c','jc','qc','kc',
//   'ad', '2d','3d','4d','5d','6d','7d','8d','9d','10d','jd','qd','kd',
//   'ah', '2h','3h','4h','5h','6h','7h','8h','9h','10h','jh','qh','kh',
//   'as', '2s','3s','4s','5s','6s','7s','8s','9s','10s','js','qs','ks'
]

const allCardsFlip = document.querySelector('.container');

allCardsFlip.addEventListener('click', function(ev){
  const currentCard = ev.target;
  currentCard.classList.add('flip');
  setTimeout(() => {
      currentCard.classList.add('card-back');
  }, 300);
  if (currentCard.classList.contains('card-back')) {
      currentCard.classList.remove('flip');
      currentCard.classList.add('flip-back');
          setTimeout(() => {
              currentCard.classList.remove('card-back');
              currentCard.classList.add('card');
          }, 300);
      currentCard.classList.remove('flip');
      currentCard.classList.remove('flip-back');
  }
})

// class Card {
//   #name
//   #value
//   constructor({name, value}) {
//       this.#name = name;
//       this.#value = value;
//   }
//   createHTMLCard = function() {
//       const card = document.createElement('div');
//       card.classList.add('card');
//       card.classList.add(`card-${this.#name}`);
//       return card;
//   }
// //   get cardName() {
// //       switch (this.suit) {
// //           case 's':
// //               return `${this.value} of spades`
// //           case 'c':
// //               return `${this.value} of clubs`
// //           case 'd':
// //               return `${this.value} of diamonds`
// //           case 'h':
// //               return `${this.value} of hearts`
// //       }
// //   }
// //   set fullValue(value) {
// //       if (allCards.indexOf(value) > -1)
// //       this.#fullValue = value;
// //   }
// }

// клас колода
class Deck {
  #deck = [];
  #drawnCards = [];
  // конструктор - перебір всіх елементів в allCards і щось там з ними зробити
  constructor() {
      allCards.forEach(cardObj => {
          const card = new Card(cardObj);
          this.#deck.push(card);
      })
  }
  // shuffle - перемішати карти
  shuffle() {
    this.#deck.sort((a,b) => Math.random() - 0.5)
  }
  // drawCard - дістати одну карту
  drawCard() {
      // this.#drawnCards.push(this.#deck.pop());
      const drawCard = this.#deck.pop();
      return drawCard
  }
  reShuffle() {
      // option 1
      this.#deck = [...this.#deck, ...this.#drawnCards];
      this.#drawnCards = [];
      // option 2
      // while(this.#drawnCards.length) {
      //     this.#deck.push(this.#drawnCards.pop());
      // }
      this.shuffle();
  }
}

const deck = new Deck();
deck.shuffle();

const btnStart = document.querySelector('.btn-start');
const btnDraw = document.querySelector('.btn-draw');
const btnReshuffle = document.querySelector('.btn-reshuffle');


let animationCard = null;
let openCardFinish;
function openOneCard() {
  const cardsDeck = document.querySelector('.cards.card-back');
  cardsDeck.classList.add('card');
  cardsDeck.classList.add('flip');
  const card = deck.drawCard();
  const openCard = new Card(card);
  openCardFinish = openCard.render()
  setTimeout(() => allCardsFlip.append(openCardFinish), 400);
  setTimeout(() => cardsDeck.classList.remove('flip'), 300);
}

function moveCard(elem, num) {
  for (let i = 1; i < 52; i++) {
      elem.classList.add('move0');
      elem.classList.add(`move${num}`);

  }
}



let num = 0;
function drawOneCard(event) {
  num++;
  openOneCard();
  switch (num) {
    case 1: 
        moveCard(openCardFinish, 3);
        break;
    case 2:
        moveCard(openCardFinish, 4);
        break;
    case 3:
        moveCard(openCardFinish, 5);
        break;
    case 4: 
        moveCard(openCardFinish, 6);
        break;
    case 5:
        moveCard(openCardFinish, 7);
        break;
    case 6:
        moveCard(openCardFinish, 8);
        break;
    case 7: 
        moveCard(openCardFinish, 9);
        break;
    case 8:
        moveCard(openCardFinish, 10);
        btnDraw.removeEventListener("click", drawOneCard);
        num = 0;
        break;
  }
}

function openTwoCards(ev) {
    debugger
  openOneCard();
  moveCard(openCardFinish, 1);
  setTimeout(() => {
      openOneCard();
      moveCard(openCardFinish, 2)
  }, 500);
  btnDraw.addEventListener('click', drawOneCard);
}

btnStart.addEventListener('click', openTwoCards, {once: true})


btnReshuffle.addEventListener('click', ev => {
  let cardsGoBack = document.querySelectorAll('.container .card');
  cardsGoBack.forEach(elem => {
      elem.remove()
  })
  openTwoCards();
  num = 0;
  btnDraw.addEventListener('click', drawOneCard);

})